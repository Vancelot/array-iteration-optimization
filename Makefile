FILE = cacheMissOpt.c
FILENAME = cacheMissOpt
ORIGFILE = initial.c
ORIGFILENAME = initial
C_FLAGS = gcc -Wall -g -o 

all: program 

program: $(FILE)
	$(C_FLAGS) $(FILENAME) $(FILE)

orig: $(ORIGFILE)
	$(C_FLAGS) $(ORIGFILENAME) $(ORIGFILE)

opt: $(FILE)
	$(C_FLAGS) $(FILENAME)Optimized $(FILE)
