#include <stdio.h>
#include <stdlib.h>

// You are only allowed to make changes to this code as specified by the comments in it.

// The code you submit must have these two values.
#define N_TIMES		600000
#define ARRAY_SIZE	 10000

int main(void)
{
	double	*array = calloc(ARRAY_SIZE, sizeof(double));
	double	sum = 0;
   double value=0;

    // putting some values in the array
    for(int i=0; i<ARRAY_SIZE; i++, value++)
         array[i] = value/1000000.0;
	// You can add variables between this comment ...
	double mySum = 0;
	// ... and this one.

	// Please change 'your name' to your actual name.
	printf("CS201 - Asgmt 4 - Kerry Vance\n");

	for(int i = 0; i < N_TIMES; i++)
	{
		for(int j = 0; j < ARRAY_SIZE; j++)
		{
			sum += array[j];
		}
	}

	// ... and this one.
    printf("sum is %10.2f\n",sum);

	 //should print 29997000.00
	free(array);
	return 0;
}
